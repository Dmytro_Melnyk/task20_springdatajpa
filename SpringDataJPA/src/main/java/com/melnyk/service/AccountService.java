package com.melnyk.service;

import com.melnyk.Repository.AccountRepository;
import com.melnyk.Repository.LaptopRepository;
import com.melnyk.domain.Account;
import com.melnyk.domain.Laptop;
import com.melnyk.exceptions.NoSuchAccountException;
import com.melnyk.exceptions.NoSuchLaptopException;
import java.util.List;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AccountService {

  @Autowired
  AccountRepository accountRepository;

  @Autowired
  LaptopRepository laptopRepository;

  public List<Account> getAccountByLaptopID(Long laptopID) throws NoSuchLaptopException {
    Laptop laptop = laptopRepository.findById(laptopID).get();
    if (laptop == null) {
      throw new NoSuchLaptopException();
    }
    return laptop.getAccounts();
  }

  public Account getAccount(Long accountID) throws NoSuchAccountException {
    Account account = accountRepository.findById(accountID).get();
    if (account == null) {
      throw new NoSuchAccountException();
    }
    return account;
  }

  public List<Account> getAllAccounts() {
    return accountRepository.findAll();
  }

  @Transactional
  public void createAccount(Account account, Long laptopID) throws NoSuchLaptopException{
    if (laptopID > 0) {
      Laptop laptop = laptopRepository.findById(laptopID).get();
      if (laptop == null) {
        throw new NoSuchLaptopException();
      }
      account.setLaptopByLaptopId(laptop);
    }
    accountRepository.save(account);
  }

  @Transactional
  public void updateAccount(Account uAccount, Long accountID, Long laptopID) throws NoSuchLaptopException, NoSuchAccountException{
    Laptop laptop = laptopRepository.findById(laptopID).get();
    if (laptopID > 0) {
      if (laptop == null) {
        throw new NoSuchLaptopException();
      }
    }
    Account account = accountRepository.findById(accountID).get();
    if (account == null) {
      throw new NoSuchAccountException();
    }
    account.setLogin(uAccount.getLogin());
    account.setPassword(uAccount.getPassword());
    if (laptopID > 0) {
      account.setLaptopByLaptopId(laptop);
    } else {
      account.setLaptopByLaptopId(null);
    }
    accountRepository.save(account);
  }

  @Transactional
  public void deleteAccount(Long accountID) throws NoSuchAccountException{
    Account account = accountRepository.findById(accountID).get();
    if (account == null) {
      throw new NoSuchAccountException();
    }
    accountRepository.delete(account);
  }
}
