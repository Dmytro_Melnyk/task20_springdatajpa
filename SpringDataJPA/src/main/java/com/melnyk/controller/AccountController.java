package com.melnyk.controller;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

import com.melnyk.DTO.AccountDTO;
import com.melnyk.domain.Account;
import com.melnyk.exceptions.NoSuchAccountException;
import com.melnyk.exceptions.NoSuchLaptopException;
import com.melnyk.service.AccountService;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Link;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class AccountController {

  @Autowired
  AccountService accountService;

  @GetMapping(value = "/api/account/laptop/{id}")
  public ResponseEntity<List<AccountDTO>> getAccountByLaptopID(@PathVariable Long laptopID) throws NoSuchLaptopException, NoSuchAccountException {
    List<Account> accountList = accountService.getAccountByLaptopID(laptopID);

    Link link = linkTo(methodOn(AccountController.class).getAllAccounts()).withSelfRel();

    List<AccountDTO> accountsDTO = new ArrayList<>();
    for (Account entity : accountList) {
      Link selfLink = new Link(link.getHref() + "/" + entity.getId()).withSelfRel();
      AccountDTO dto = new AccountDTO(entity, selfLink);
      accountsDTO.add(dto);
    }
    return new ResponseEntity<>(accountsDTO, HttpStatus.OK);
  }

  @GetMapping(value = "/api/account/{id}")
  public ResponseEntity<AccountDTO> getAccount(@PathVariable Long accountID) throws NoSuchAccountException {
    Account account = accountService.getAccount(accountID);
    Link link = linkTo(methodOn(AccountController.class).getAccount(accountID)).withSelfRel();

    AccountDTO accountDTO = new AccountDTO(account, link);

    return new ResponseEntity<>(accountDTO, HttpStatus.OK);
  }

  @GetMapping(value = "/api/account")
  private ResponseEntity<List<AccountDTO>> getAllAccounts() throws NoSuchAccountException {
    List<Account> accountList = accountService.getAllAccounts();
    Link link = linkTo(methodOn(AccountController.class).getAllAccounts()).withSelfRel();

    List<AccountDTO> accountsDTO = new ArrayList<>();
    for (Account entity : accountList) {
      Link selfLink = new Link(link.getHref() + "/" + entity.getId()).withSelfRel();
      AccountDTO dto = new AccountDTO(entity, selfLink);
      accountsDTO.add(dto);
    }
    return new ResponseEntity<>(accountsDTO, HttpStatus.OK);
  }

  @PostMapping(value = "/api/account/laptop/{id}")
  public  ResponseEntity<AccountDTO> addPerson(@RequestBody Account newAccount, @PathVariable Long accountID)
      throws NoSuchLaptopException, NoSuchAccountException {
    accountService.createAccount(newAccount, accountID);
    Link link = linkTo(methodOn(AccountController.class).getAccount(newAccount.getId())).withSelfRel();

    AccountDTO accountDTO = new AccountDTO(newAccount, link);

    return new ResponseEntity<>(accountDTO, HttpStatus.CREATED);
  }

  @PutMapping(value = "/api/account/{id}/laptop/{id}")
  public  ResponseEntity<AccountDTO> updatePerson(@RequestBody Account uAccount,
      @PathVariable Long accountID, @PathVariable Long laptopID)
      throws NoSuchLaptopException, NoSuchAccountException {
    accountService.updateAccount(uAccount, accountID, laptopID);
    Account account = accountService.getAccount(accountID);
    Link link = linkTo(methodOn(AccountController.class).getAccount(accountID)).withSelfRel();

    AccountDTO accountDTO = new AccountDTO(account, link);

    return new ResponseEntity<>(accountDTO, HttpStatus.OK);
  }

  @DeleteMapping(value = "/api/account/{id}")
  public  ResponseEntity deletePerson(@PathVariable Long accountID) throws NoSuchAccountException {
    accountService.deleteAccount(accountID);
    return new ResponseEntity(HttpStatus.OK);
  }
}
