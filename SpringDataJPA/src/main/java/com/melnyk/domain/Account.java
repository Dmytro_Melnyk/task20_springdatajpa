package com.melnyk.domain;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "account", schema = "laptops_db")
public class Account {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  @Column(name = "id", nullable = false)
  private Long id;

  @Basic
  @Column(name = "login", nullable = false, length = 45)
  private String login;


  @Basic
  @Column(name = "password", nullable = false, length = 45)
  private String password;

  @ManyToOne
  @JoinColumn(name = "laptop_id", referencedColumnName = "id", nullable = false)
  private Laptop laptopByLaptopId;

  public Account() {
  }

  public Account(String login) {
    this.login = login;
  }

  public Account(String login, String password) {
    this.login = login;
    this.password = password;
  }

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getLogin() {
    return login;
  }

  public void setLogin(String login) {
    this.login = login;
  }

  public String getPassword() {
    return password;
  }

  public void setPassword(String password) {
    this.password = password;
  }

  public Laptop getLaptopByLaptopId() {
    return laptopByLaptopId;
  }

  public void setLaptopByLaptopId(Laptop laptopByLaptopId) {
    this.laptopByLaptopId = laptopByLaptopId;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }

    Account that = (Account) o;

    if (id != null ? !id.equals(that.id) : that.id != null) {
      return false;
    }
    if (login != null ? !login.equals(that.login) : that.login != null) {
      return false;
    }
    if (password != null ? !password.equals(that.password)
        : that.password != null) {
      return false;
    }

    return true;
  }

  @Override
  public int hashCode() {
    int result = id != null ? id.hashCode() : 0;
    result = 31 * result + (login != null ? login.hashCode() : 0);
    result = 31 * result + (password != null ? password.hashCode() : 0);
    return result;
  }
}
